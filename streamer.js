const { spawn } = require('child_process');

const kill = require('tree-kill');

const express = require('express');
const app = express();
const server = require('http').createServer(app);
const io = require('socket.io')(server);

let spawnCommand = null;

const rtmpStreamUri = 'rtmp://localhost/live/test';

// ffmpeg command in cmd
const cmd = 'ffmpeg';
const args = [
  '-f', 'dshow',
  '-i', 'video=Integrated Camera',
  '-b:v', '1000k',
  '-s', '640x480',
  '-c:v', 'h264',
  '-preset', 'ultrafast',
  '-f', 'flv', rtmpStreamUri
];

io.on('connection', (socket) => {
  socket.on('stream-start', (data) => {
    console.log('Stream has been started!');
    spawnCommand = spawn(cmd, args, { stdio: 'inherit' });
  });

  socket.on('stream-stop', () => {
    if (spawnCommand != null) {
      console.log(`Stream has been terminated!\nRtmp output uri is ${rtmpStreamUri}`);
      kill(spawnCommand.pid);
    }
  });
});

app.get('/', (req, res) => {
  res.sendFile('streamer.html', { root: __dirname });
});

server.listen(5000);

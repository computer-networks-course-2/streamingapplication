const NodeMediaServer = require('node-media-server');
const express = require('express');
const app = express();
const path = require('path');

app.use(express.static(path.join(__dirname, '/')));

// Ports
const RTMP_PORT = 1935;
const HTTP_OUTPUT_PORT = 8000;
const LISTEN_PORT = 8080;

// Node Media Server Config
const config = {
  rtmp: {
    port: RTMP_PORT,
    chunk_size: 60000,
    gop_cache: true,
    ping: 20,
    ping_timeout: 60
  },
  http: {
    port: HTTP_OUTPUT_PORT,
    allow_origin: '*'
  }
};

var nms = new NodeMediaServer(config);

nms.run();

// Stream access for clients
app.get('/', (req, res) => {
  res.sendFile('index.html');
});

app.listen(LISTEN_PORT);

# StreamingApplication
This is a simple streaming system with a server and client made in HTML and NodeJS.

## Requirements
In order to use the application, you need to have installed NodeJS and ffmpeg.

## Setup
After cloning the project, execute the command 'npm install' to install all the needed packages.

## Run
Use two terminals, one for starting the server and one for starting the streamer.
Execute 'node server' and then 'node streamer'.

Access the streamer on localhost:5000.
Access the client on localhost:8080.